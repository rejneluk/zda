import numpy as np
import pandas as pd
import csv
import os


class RowIden:
    def __init__(self, procName, typPodani, typZadosti, timeCol, cRange, subProc=False):
        self.procName = procName
        self.typPod = typPodani
        self.typZad = typZadosti
        self.timeCol = timeCol
        self.cRange = cRange
        self.subProc = subProc


def writeToFile(row):
    for i in range(len(row)):
        if pd.isnull(row[i]):
            row[i] = ""

    file = open(filename, "a")
    write = csv.writer(file, delimiter='\t')
    write.writerow(row)


def readTime(df, orgRow, customRange, timeColumn, subProc=False):
    proces = ""
    activity = ""

    for i in customRange:
        row = orgRow.copy()

        if "." not in str(df.loc[i, 0]):
            proces = df.loc[i, 0]
            activity = df.loc[i, 1]

        row[0] = proces  # proces
        if subProc:
            row[1] = df.loc[i, 0]  # podproces
            row[3] = df.loc[i, 1] # podaktivita

        row[2] = activity # aktivita
        row[6] = df.loc[i, timeColumn]  # cas
        row[8] = df.loc[i, 2]  # role

        if row[8] is not np.nan:
            writeToFile(row)


def readFromClass(df, orgRow, rowIdens):
    for r in rowIdens:
        row = orgRow.copy()
        row[10] = r.typZad
        row[7] = r.typPod
        row[9] = r.procName
        readTime(df, row, r.cRange, r.timeCol, r.subProc)


def readFromOneTable(df, orgRow):
    readData = [
        RowIden("Podání žádosti", "Fyzicky podaná žádost", "nekompletní žádost", 3, range(3, 44), True),
        RowIden("Podání žádosti", "Fyzicky podaná žádost", "kompletní žádost", 4, range(3, 44), True),
        RowIden("Podání žádosti", "Datová schránka", "nekompletní žádost", 7, range(3, 44), True),
        RowIden("Podání žádosti", "Datová schránka", "kompletní žádost", 8, range(3, 44), True),
        RowIden("Podání žádosti", "Robot", "nezadaná žádost - vložená do datové schránky", 10, range(3, 44), True),
        RowIden("Podání žádosti", "Robot", "částečně zadaná žádost", 11, range(3, 44), True),
        RowIden("Podání žádosti", "Robot", "úplně zadaná žádost", 12, range(3, 44), True),

        RowIden("Vyhodnocení žádosti", np.nan, np.nan, 3, range(49, 56)),

        RowIden("Další změny v žádosti", np.nan, np.nan, 3, range(76, 80)),

        RowIden("Změny žádosti", "Fyzicky podaná žádost", "nekompletní žádost", 4, range(62, 71)),
        RowIden("Změny žádosti", "Fyzicky podaná žádost", "kompletní žádost", 5, range(62, 71)),
        RowIden("Změny žádosti", "Datová schránka", "nekompletní žádost", 8, range(62, 71)),
        RowIden("Změny žádosti", "Datová schránka", "kompletní žádost", 9, range(62, 71)),
        RowIden("Změny žádosti", "Robot", "nezadaná žádost - vložená do datové schránky", 11, range(62, 71)),
        RowIden("Změny žádosti", "Robot", "částečně zadaná žádost", 12, range(62, 71)),
        RowIden("Změny žádosti", "Robot", "úplně zadaná žádost", 13, range(62, 71))
    ]

    readFromClass(df, orgRow, readData)


filename = "test.csv"
file = open(filename, "w")
file.write("Proces\tPodproces\tAktivity\tPodAktivity\tZdroj\tKraj\tCas\tTyp podani\tRole\tNazev procesu\tTyp zadosti\n")
file.close()

# pro kazdy kraj
for krajDir in os.listdir("./Pruzkum_data"):
    deftRow = [np.nan] * 11
    deftRow[5] = krajDir

    # pro kazdy excel soubor
    for file in os.listdir("./Pruzkum_data/" + krajDir):
        if file[0] == ".":  # hiden files
            continue

        print(file)
        deftRow[4] = file
        df = pd.read_excel("./Pruzkum_data/" + krajDir + "/" + file, header=None, index_col=None)
        readFromOneTable(df, deftRow)
